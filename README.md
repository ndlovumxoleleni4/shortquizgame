# Quiz Game

## Flow
* Asks the user general random questions.
* Check their answer against the correct answer.
* Tells the user if they got the correct answer or not.
* Determines and displays the score of the user.

## Funcionality
* The StartQuiz function is called in the Main method and it is responsible for the overall flow of the quiz. It calls the AskQuestion function to display the question, the CheckAnswer function to check the user's answer, and the ShowScore function to display the final score.

* The AskQuestion function takes an integer as input, which is the index of the question in the list of questions, and it displays the corresponding question on the console.

* The CheckAnswer function takes the user's input, compares it with the correct answer, and displays a message indicating whether the answer is correct or not. If the answer is correct it increments the score.

* The ShowScore function displays the final score on the console.


## To Run The Application:
    dotnet run

## To Run All The Tests:
    dotnet test

## Thank You!