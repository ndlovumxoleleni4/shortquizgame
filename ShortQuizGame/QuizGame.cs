﻿namespace ShortQuizGame
{
    public class QuizGame
    {
        public static int score = 0; // to keep the score

        public static List<Tuple<string, string>> questions = new List<Tuple<string, string>>() // list of questions
        {
            Tuple.Create("What is the capital of France?", "Paris"),
            Tuple.Create("What is the largest planet in our solar system?", "Jupiter"),
            Tuple.Create("Which animal is known as the 'king of the jungle'?", "Lion"),
            Tuple.Create("Which element has the chemical symbol 'O'?", "Oxygen")
        };


        static void Main(string[] args)
        {
            StartQuiz();
        }


        public static void StartQuiz()
        {
            for (int i = 0; i < questions.Count; i++)
            {
                Console.WriteLine(AskQuestion(i));
                Console.WriteLine(CheckAnswer(i));
            }
            Console.WriteLine(ShowScore());
        }


        public static string AskQuestion(int i)
        {
            return questions[i].Item1;
        }


        public static string CheckAnswer(int i)
        {
            string? answer = Console.ReadLine();
            var question = questions[i];
            if (answer?.ToLower() == question.Item2.ToLower())
            {
                score++;
                return "Correct!";
            }
            else
            {
                return "Incorrect. The correct answer is " + question.Item2;
            }
        }


        public static string ShowScore()
        {
            return "Your final score is: " + score + " out of " + questions.Count;
        }
    }
}