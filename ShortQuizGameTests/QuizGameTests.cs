using ShortQuizGame;

namespace ShortQuizGameTests
{
    public class QuizGameTests
    {
        [SetUp]
        public void SetUp()
        {
            // Initial value.
            QuizGame.score = 0;
        }


        [TearDown]
        public void TearDown()
        {
            QuizGame.score = 0;
        }


        [Test]
        public void StartQuiz_WhenCalled_ShouldAskAllQuestions()
        {
            Assert.AreEqual("What is the capital of France?", QuizGame.AskQuestion(0));
        }


        [Test]
        public void CheckAnswer_WhenCalled_ShouldIncrementScore()
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                using (StringReader sr = new StringReader("Paris\n"))
                {
                    Console.SetIn(sr);
                    var res = QuizGame.CheckAnswer(0);
                    Assert.That(1, Is.EqualTo(QuizGame.score));
                    Assert.That("Correct!", Is.EqualTo(res));
                }
            }
        }


        [Test]
        public void CheckAnswer_WhenCalledWithWrongAnswer_ShouldNotIncrementScore()
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                using (StringReader sr = new StringReader("Pariss\n"))
                {
                    Console.SetIn(sr);
                    var res = QuizGame.CheckAnswer(0);
                    Assert.That(0, Is.EqualTo(QuizGame.score));
                    Assert.That("Incorrect. The correct answer is Paris", Is.EqualTo(res));
                }
            }
        }
    }
}